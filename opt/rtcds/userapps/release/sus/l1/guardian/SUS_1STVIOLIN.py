from guardian import GuardState
from guardian import NodeManager
from time import sleep
import subprocess
import time
import os
import cdsutils

# v1 - S. Aston - 04/01/2019
# Script for LLO to manage SUS violin modes

# v2 - S. Aston - 04/03/2019
# Added DAMP ALL and UNDAMP ALL states
# Added some notifications for completing DAMP and UNDAMP all completion and if the overall VM damping switch is off

# v3 - S. Aston - 04/04/2019
# Enabled "goto" states for each specific violin mode order damping or un-damping, but leave damp/undamp all as requestable states only

# v4 - S. Aston - 04/09/2019
# Make almost everything a "goto" state, make "damp_3rd" nominal and remove "damp_all"
# Reduced notify sleeps from 5 to 3 seconds to help expedite things

# v5 - S. Aston - 04/11/2019
# Based on SUS_VIOLIN.py generated a dedicated Guardian node for just 1st order violin mode damping

# v6 - S. Aston - 05/21/2019
# Added violin mode warnings to monitor damping of modes above a defined threshold

# v7 - S. Aston - 06/25/2019
# Enabled violin mode monitoring to automatically shut down damping of modes above a defined threshold to try and avoid a lock-loss

# v8 - C. Blair - 01/25/2022
# Enabled violin mode monitoring to automatically switch to state UNDAMP_1st for transparent safety shutoff. 

# TODO:
# Consider adding some alarms and alerts after reaching a threshold?
# Can we speed up setting up filters by restoring EDB snapshots of violin modes?

# Define all QUAD suspensions
quads = ['ITMX','ITMY','ETMX','ETMY']

# Define 1st 2nd and 3rd order mode numbers
modes1 = ['3', '4', '5', '6']
modes2 = ['11', '12', '13', '14']
modes3 = ['21', '22', '23', '24']
modesall = ['1','2','7','8','9','10','15','16','17','18','19','20','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40']

# Nominal operational state
nominal = 'DAMP_1ST'
#nominal = 'IDLE'

# Initial request on initialization
request = 'IDLE'

class INIT(GuardState):
    def main(self):
        return True

class IDLE(GuardState):
    goto = True
    request = True
    
    def main(self):
        return True

class DAMP_1ST(GuardState):
    goto = True
    request = True
    
    def main(self):        
        #if ezca['SUS-ITMX_L2_DAMP_OUT_SW'] == 0:
        #    notify("Violin mode damping switch is disabled")

        for sus in quads:
            for mode in modes1:
		# Zero the gains for all QUADs, just in case damping is already engaged
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_GAIN'] = 0
        sleep(3.0)

        for sus in quads:
            for mode in modes1:
                # Clear damping filter histories for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_RSET'] = 2
                # Clear monitor filter histories for all QUADs, commented out because it is not necessary
                #ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_BL_RSET'] = 2
                #ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_RMSLP_RSET'] = 2

        # Set ITMX damping filters
        ezca.switch('SUS-ITMX_L2_DAMP_MODE3', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE3', 'FM3', 'FM10', 'ON') # Removed FM5, added FM1 & FM6 09/16/2023 by DL #changed by TJ

        ezca.switch('SUS-ITMX_L2_DAMP_MODE4', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE4', 'FM6', 'FM10', 'ON')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE5', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE5', 'FM3', 'FM10', 'ON') #Changed 09/05/2023 disengaged FM1 by DL

        ezca.switch('SUS-ITMX_L2_DAMP_MODE6', 'FMALL', 'OFF')

        # Set ITMY damping filters
        ezca.switch('SUS-ITMY_L2_DAMP_MODE3', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE3', 'FM1', 'FM6', 'FM10', 'ON')  # Best recent filter settings since O4 vent, DL 4/01/2024
        
        ezca.switch('SUS-ITMY_L2_DAMP_MODE4', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE4', 'FM1', 'FM3', 'FM10', 'ON') # New filter settings since O4 vent, DL 2/23/2024

        ezca.switch('SUS-ITMY_L2_DAMP_MODE5', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE5', 'FM1', 'FM6', 'FM10', 'ON')  # Best recent filter settings since O4 vent, DL 4/01/2024

        ezca.switch('SUS-ITMY_L2_DAMP_MODE6', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE6', 'FM2', 'FM3', 'FM10', 'ON') # New filter settings since O4 vent, DL 4/02/2024, changed from FM1 to FM2 12/14/2924 EM


        # Set ETMX damping filters
        ezca.switch('SUS-ETMX_L2_DAMP_MODE3', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE3', 'FM3', 'FM10', 'ON') 

        ezca.switch('SUS-ETMX_L2_DAMP_MODE4', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE4', 'FM1', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE5', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE5', 'FM1', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE6', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE6', 'FM3' , 'FM10', 'ON')


        # Set ETMY damping filters
        ezca.switch('SUS-ETMY_L2_DAMP_MODE3', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE3', 'FM2', 'FM4', 'FM10', 'ON') # New filter settings since O4 vent, DL 2/23/2024

        ezca.switch('SUS-ETMY_L2_DAMP_MODE4', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE4', 'FM1', 'FM3','FM10', 'ON')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE5', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE5', 'FM5','FM10', 'ON') # Changed FM3 to FM5 on 09/16/2023 by DL

        ezca.switch('SUS-ETMY_L2_DAMP_MODE6', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE6', 'FM5', 'FM10', 'ON') # New filter settings since O4 vent, DL 3/22/2024


        # Engage damping filter outputs for all QUADs
        for sus in quads:
            for mode in modes1:
                ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'ON')

        # Set ITMX damping filter gains
        ezca['SUS-ITMX_L2_DAMP_MODE3_GAIN'] = -100
        ezca['SUS-ITMX_L2_DAMP_MODE4_GAIN'] = 10
        ezca['SUS-ITMX_L2_DAMP_MODE5_GAIN'] = 20
        ezca['SUS-ITMX_L2_DAMP_MODE6_GAIN'] = 0

        # Set ITMY damping filter gains
        ezca['SUS-ITMY_L2_DAMP_MODE3_GAIN'] = 0 # Default setting -30 since O4 vent, set to 0 at startup DL 8/01/2024
        ezca['SUS-ITMY_L2_DAMP_MODE4_GAIN'] = 0 # Default setting -5 since O4 vent, set to 0 at startup DL 12/23/2024,changed to -2 12/14/2024 EM
        ezca['SUS-ITMY_L2_DAMP_MODE5_GAIN'] = 0 # Changed gain from 2 to 0 at startup DL 12/23/2024,changed to 2 12/14/2024 EM
        ezca['SUS-ITMY_L2_DAMP_MODE6_GAIN'] = 0 # Changed gain from 10 to 0 at startup DL 7/19/2024

        # Set ETMX damping filter gains
        ezca['SUS-ETMX_L2_DAMP_MODE3_GAIN'] = 20
        ezca['SUS-ETMX_L2_DAMP_MODE4_GAIN'] = 20
        ezca['SUS-ETMX_L2_DAMP_MODE5_GAIN'] = -20
        ezca['SUS-ETMX_L2_DAMP_MODE6_GAIN'] = 10 # Changed AE 230802

        # Set ETMY damping filter gains
        ezca['SUS-ETMY_L2_DAMP_MODE3_GAIN'] = 20 # Default setting 20 since O4 vent, set to 0 at startup DL 2/23/2024
        ezca['SUS-ETMY_L2_DAMP_MODE4_GAIN'] = 10
        ezca['SUS-ETMY_L2_DAMP_MODE5_GAIN'] = 20 # Changed gain from -20 to 20 by DL 09/06/2023
        ezca['SUS-ETMY_L2_DAMP_MODE6_GAIN'] = 20 # Default setting 20 since O4 vent, set to 0 at startup DL 2/23/2024
        
        notify("1st order violin mode damping engaged")
        sleep(3.0)

    def run(self):
        for sus in quads:
            for mode in modes1:
                level = ezca['SUS-'+sus+'_L2_DAMP_MODE'+mode+'_RMSLP_LOG10_OUTMON']
                index = ezca['GRD-ISC_LOCK_STATE_N']
                gain = ezca['SUS-'+sus+'_L2_DAMP_MODE'+mode+'_GAIN'] # added by TJ on 12/29/2023
                if (level >= -16.5) & (index >= 1100):  # Please do not change threshold level, should remain at -16.5!
                    if gain == 0: # added by TJ on 12/29/2023
                        if not ((sus == 'ITMX') & (mode == '6')):
                            notify("Warning! "+sus+" violin mode "+mode+" above threshold, gain set to zero")
                        continue # added by TJ on 12/29/2023
                    else:
                        #notify("Warning! "+sus+" violin mode "+mode+" elevated")
                        notify("Warning! "+sus+" violin mode "+mode+" above threshold, gain set to zero")                        
                        ezca['SUS-'+sus+'_L2_DAMP_MODE'+mode+'_GAIN'] = 0 #enabled on 10/18/2023 DL
                        #return True
        return True

class UNDAMP_1ST(GuardState):
    goto = True
    request = True
    
    def main(self):
        for sus in quads:
            for mode in modes1:
                # Zero the gains for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_GAIN'] = 0
                # Disengage damping filter outputs for all QUADs
                ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'OFF')
        notify("1st order violin mode damping disengaged")
        sleep(3.0)

    def run(self):
        return True

class UNDAMP_ALL(GuardState):
    goto = True
    request = True

    def main(self):
        for sus in quads:
            #for mode in modes1 + modes2 + modes3 + modesall:
            for mode in range(40):
                # Zero the gains for all QUADs
                #ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_GAIN'] = 0
                ezca.write('SUS-{}_L2_DAMP_MODE{}_GAIN'.format(sus,mode+1),0)
                # Disengage damping filter outputs for all QUADs
                #ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'OFF')
                ezca.switch('SUS-{}_L2_DAMP_MODE{}'.format(sus,mode+1),'OUTPUT','OFF')
        notify("All violin mode damping disengaged")
        sleep(3.0)

    def run(self):
        return True

edges = [
    ('INIT','IDLE'),
    ('IDLE','DAMP_1ST'),
    ('IDLE','UNDAMP_1ST'),
    ('IDLE','UNDAMP_ALL'),
    ]

# edges += [('DAMP_1ST','DAMP_1ST')]
edges += [('UNDAMP_1ST','UNDAMP_1ST')]
edges += [('UNDAMP_ALL','UNDAMP_ALL')]
